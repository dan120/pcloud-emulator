// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package model

type Response struct {
	Result int64   `json:"result"`
	Error  *string `json:"error,omitempty"`
}

func CreateErrorResponse(result int64, err *string) Response {
	return Response{Result: result, Error: err}
}
