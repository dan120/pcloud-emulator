// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package model

type CreateUploadLinkResponse struct {
	UploadLinkId int64  `json:"uploadlinkid"`
	Link         string `json:"link,omitempty"`
	Mail         string `json:"mail,omitempty"`
	Code         string `json:"code,omitempty"`
	Result       int64  `json:"result"`
	Error        int64  `json:"error,omitempty"`
}

type GetFilePubLinkResponse struct {
	LinkId int64  `json:"linkid"`
	Code   string `json:"code"`
}
