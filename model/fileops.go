// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package model

type FileDescriptor struct {
	Id       int64
	FileId   int64
	FilePath string
	Flags    string
}

type FileOpenResponse struct {
	Result int64 `json:"result"`
	Fd     int64 `json:"fd"`
	FileId int64 `json:"fileid"`
}

type FileSizeResponse struct {
	Result int64
	Size   int64
	Offset int64
}

type FileCloseResponse struct {
	Result int64
}
