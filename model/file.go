// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package model

type UploadFileResponse struct {
	Result    int64       `json:"result"`
	FileIds   []int64     `json:"fileids"`
	Checksums []Checksums `json:"checksums"`
	Metadata  []Metadata  `json:"metadata"`
}

type Checksums struct {
	SHA1   string `json:"sha1,omitempty"`
	SHA256 string `json:"sha256,omitempty"`
}
