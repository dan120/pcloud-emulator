// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package model

type Metadata struct {
	Created        string     `json:"created,omitempty"`
	IsFolder       bool       `json:"isfolder"`
	ParentFolderId int64      `json:"parentfolderid"`
	Icon           string     `json:"icon,omitempty"`
	Id             string     `json:"id,omitempty"`
	FileId         *int64     `json:"fileid,omitempty"`
	FullPath       string     `json:"-"`
	Path           string     `json:"path,omitempty"`
	Modified       string     `json:"modified,omitempty"`
	Thumb          bool       `json:"thumb"`
	FolderId       *int64     `json:"folderid,omitempty"`
	IsShared       bool       `json:"isshared"`
	IsMine         bool       `json:"ismine"`
	Name           string     `json:"name,omitempty"`
	Size           int64      `json:"size,omitempty"`
	Contents       []Metadata `json:"contents,omitempty"`
}

type MetadataResponse struct {
	Result   int64     `json:"result"`
	Metadata *Metadata `json:"metadata,omitempty"`
	Error    *string   `json:"error,omitempty"`
}

func (m *MetadataResponse) SetResponse(r int64) {
	m.Result = r
}

func (m *MetadataResponse) SetError(e string) {
	m.Error = &e
}
