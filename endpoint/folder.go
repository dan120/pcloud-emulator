// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package endpoint

import (
	"log/slog"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/icza/gog"
	"gitlab.com/dan120/pcloud-emulator/archive"
	"gitlab.com/dan120/pcloud-emulator/model"
)

type Folder struct {
	Archive archive.ArchiveService
}

func NewFolder(a archive.ArchiveService) Folder {
	return Folder{
		Archive: a,
	}
}

func (f *Folder) CreateFolderEndpoint(c *gin.Context) {
	var response model.MetadataResponse
	var err error

	var name string = c.Query("name")
	var parentId *int64 = GetInt64Value(c, "folderid")

	// validate params
	if name == "" || parentId == nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1000, gog.Ptr("No name or folderid provided.")))
		return
	}

	// perform action
	response.Metadata, err = f.Archive.CreateFolder(*parentId, name)
	if err != nil {
		slog.Error("Error creating folder", "parentId", parentId, "name", name, "error", err)
		c.JSON(http.StatusOK, model.CreateErrorResponse(5000, gog.Ptr(err.Error())))
		return
	}

	// return response
	c.JSON(http.StatusOK, response)
}

func (f *Folder) CreateFolderIfNotExistsEndpoint(c *gin.Context) {
	var response model.MetadataResponse
	var err error

	var name string = c.Query("name")
	var parentId *int64 = GetInt64Value(c, "folderid")

	// validate params
	if name == "" || parentId == nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1001, gog.Ptr("No name or folderid provided.")))
		return
	}

	// perform action
	response.Metadata, err = f.Archive.CreateFolderIfNotExists(*parentId, name)
	if err != nil {
		slog.Error("Error creating folder", "parentId", parentId, "name", name, "error", err)
		c.JSON(http.StatusOK, model.CreateErrorResponse(5000, gog.Ptr(err.Error())))
		return
	}

	// return response
	c.JSON(http.StatusOK, response)
}

func (f *Folder) ListFolderEndpoint(c *gin.Context) {
	var response model.MetadataResponse
	var err error

	var path string = c.Query("path")
	var folderId *int64 = GetInt64Value(c, "folderid")

	// validate params
	if folderId == nil && path == "" {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1001, gog.Ptr("No path or folderid provided.")))
		return
	}

	var meta *model.Metadata

	if folderId != nil {
		meta, err = f.Archive.ListFolderWithFolderId(*folderId)
	} else {
		meta, err = f.Archive.ListFolderWithPath(path)
	}
	if err != nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1001, gog.Ptr(err.Error())))
		return
	}

	response.Metadata = meta

	// return response
	c.JSON(http.StatusOK, response)
}
