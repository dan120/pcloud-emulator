// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package endpoint

import (
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/icza/gog"
	"gitlab.com/dan120/pcloud-emulator/archive"
	"gitlab.com/dan120/pcloud-emulator/model"
)

type FileOps struct {
	Archive    archive.ArchiveService
	FileIdMap  map[int64]model.FileDescriptor
	NextFileId int64
}

func NewFileOps(a archive.ArchiveService) FileOps {
	return FileOps{
		Archive:    a,
		FileIdMap:  make(map[int64]model.FileDescriptor),
		NextFileId: 1,
	}
}

func (f *FileOps) FileOpenEndpoint(c *gin.Context) {
	var m *model.Metadata
	var found bool

	// make sure flags were passed
	flags := c.Query("flags")
	if flags == "" {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1006, gog.Ptr("Please provide flags.")))
		return
	}

	// get path and fileId params
	path := c.Query("path")
	fileId := GetInt64Value(c, "fileid")

	// try to find the file being references
	if fileId != nil {
		m, found = f.Archive.StatWithFileId(*fileId)
	} else {
		m, found = f.Archive.StatWithPath(path)
	}

	// only process if it is a found file
	if !found || m.IsFolder {
		c.JSON(http.StatusOK, model.CreateErrorResponse(2009, gog.Ptr("File not found.")))
		return
	}

	// build & save file descriptor
	fd := model.FileDescriptor{
		Id:       f.NextFileId,
		FileId:   *m.FileId,
		FilePath: m.FullPath,
		Flags:    flags,
	}
	f.FileIdMap[fd.Id] = fd
	f.NextFileId = f.NextFileId + 1

	// send response
	c.JSON(http.StatusOK, &model.FileOpenResponse{
		Fd:     fd.Id,
		FileId: fd.FileId,
	})
}

func (f *FileOps) FileSizeEndpoint(c *gin.Context) {
	// attempt to locate file descriptor
	var fd *model.FileDescriptor = f.getFileDescriptor(GetInt64Value(c, "fd"))

	// fail if not found
	if fd == nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1007, gog.Ptr("Invalid or closed file descriptor.")))
		return
	}

	// stat the file pointed to by file descriptor
	fi, err := os.Stat(fd.FilePath)
	if err != nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(5000, gog.Ptr(err.Error())))
		return
	}

	// return response
	c.JSON(http.StatusOK, model.FileSizeResponse{
		Size: fi.Size(),
	})
}

func (f *FileOps) FilePreadEndpoint(c *gin.Context) {
	// attempt to locate file descriptor
	var fd *model.FileDescriptor = f.getFileDescriptor(GetInt64Value(c, "fd"))

	// fail if not found
	if fd == nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1007, gog.Ptr("Invalid or closed file descriptor.")))
		return
	}

	// get count and offset
	count := GetInt64Value(c, "count")
	offset := GetInt64Value(c, "offset")
	if offset == nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1009, gog.Ptr("Please provide 'offset'.")))
		return
	}
	if count == nil {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1011, gog.Ptr("Please provide 'count'.")))
		return
	}

	// read the file
	file, err := os.Open(fd.FilePath)
	if err != nil {
		slog.Error("Error opening file", "filePath", fd.FilePath, "error", err)
		c.JSON(http.StatusOK, model.CreateErrorResponse(5004, gog.Ptr("Read error. Try reopening the file.")))
		return
	}
	defer file.Close()

	// seek to offset in file
	_, err = file.Seek(*offset, io.SeekStart)
	if err != nil {
		slog.Error("Error seeking file", "filePath", fd.FilePath, "offset", *offset, "count", *count, "error", err)
		c.JSON(http.StatusOK, model.CreateErrorResponse(5004, gog.Ptr("Read error. Try reopening the file.")))
		return
	}

	// read the specified number of bytes
	buf := make([]byte, *count)
	readCount, err := file.Read(buf)
	if err != nil {
		slog.Error("Error reading file", "filePath", fd.FilePath, "count", *count, "error", err)
		c.JSON(http.StatusOK, model.CreateErrorResponse(5004, gog.Ptr("Read error. Try reopening the file.")))
		return
	}

	c.Header("Content-Size", fmt.Sprintf("%d", readCount))
	c.Writer.Write(buf)
}

func (f *FileOps) FileCloseEndpoint(c *gin.Context) {
	// remove FileDescriptor from map
	fid := GetInt64Value(c, "fd")
	if fid != nil {
		delete(f.FileIdMap, *fid)
	}

	// return response
	c.JSON(http.StatusOK, model.FileCloseResponse{})
}

func (f *FileOps) getFileDescriptor(fid *int64) *model.FileDescriptor {
	// attempt to locate file descriptor
	var fd *model.FileDescriptor
	if fid != nil {
		d, found := f.FileIdMap[*fid]
		if found {
			fd = &d
		}
	}
	return fd
}
