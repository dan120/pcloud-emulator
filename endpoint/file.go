// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package endpoint

import (
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/icza/gog"
	"github.com/mostlygeek/parhash"
	"gitlab.com/dan120/pcloud-emulator/archive"
	"gitlab.com/dan120/pcloud-emulator/model"
)

type File struct {
	Archive archive.ArchiveService
}

func NewFile(a archive.ArchiveService) File {
	return File{Archive: a}
}

func (f *File) StatEndpoint(c *gin.Context) {
	var response model.MetadataResponse

	var fileId *int64 = GetInt64Value(c, "fileid")
	var path = c.Query("path")

	// validate params
	if fileId == nil && path == "" {
		c.JSON(http.StatusOK, model.CreateErrorResponse(1001, gog.Ptr("No path or fileid provided.")))
		return
	}

	// perform action
	var m *model.Metadata
	var found bool

	if fileId != nil {
		m, found = f.Archive.StatWithFileId(*fileId)
	} else {
		m, found = f.Archive.StatWithPath(path)
	}

	if !found {
		c.JSON(http.StatusOK, model.CreateErrorResponse(2009, gog.Ptr("File not found.")))
		return
	}

	response.Metadata = m

	// return response
	c.JSON(http.StatusOK, response)
}

func (f *File) UploadFileEndpoint(c *gin.Context) {
	fileIds := make([]int64, 0)
	metadatas := make([]model.Metadata, 0)
	checksums := make([]model.Checksums, 0)

	// determine target path for upload
	var folderId *int64 = GetInt64Value(c, "folderid")
	var path = c.Query("path")

	var m *model.Metadata
	var found bool

	if folderId != nil {
		m, found = f.Archive.StatWithFileId(*folderId)
	} else {
		m, found = f.Archive.StatWithPath(path)
	}

	if !found {
		c.JSON(http.StatusOK, model.CreateErrorResponse(2009, gog.Ptr("File not found.")))
		return
	}

	dstFolder := m.FullPath

	// only handles single file upload for now and must use form field "file"
	file, err := c.FormFile("file")
	if err != nil {
		slog.Error("Error uploading file", "error", err)
	}
	if file != nil {
		slog.Debug("Saving uploaded file", "dstFolder", dstFolder, "filename", file.Filename)
		dstPath := filepath.Join(dstFolder, file.Filename)
		err := c.SaveUploadedFile(file, dstPath)
		if err != nil {
			slog.Error("Error saving uploaded file", "error", err)
		}
		var found bool
		var nm *model.Metadata
		for i := 0; i < 4 && !found; i++ {
			nm, found = f.Archive.StatWithFullPath(dstPath)
			if !found {
				time.Sleep(500 * time.Millisecond)
			}
		}
		if !found || nm == nil {
			slog.Error("Unable to stat newly uploaded file")
		} else if nm != nil {
			metadatas = append(metadatas, *nm)
			fileIds = append(fileIds, *nm.FileId)
			csm, err := f.calculateChecksums(dstPath)
			if err == nil {
				checksums = append(checksums, csm)
			} else {
				slog.Error("Error calculating checksums", "error", err, "filename", dstPath)
			}
		}
	} else {
		slog.Error("Unable to find any uploaded files")
	}

	// return response
	c.JSON(http.StatusOK, model.UploadFileResponse{FileIds: fileIds, Metadata: metadatas, Checksums: checksums})
}

func (f *File) calculateChecksums(path string) (model.Checksums, error) {
	var response model.Checksums

	p := parhash.New()
	hash1 := p.Add(sha1.New())
	hash2 := p.Add(sha256.New())

	b, err := os.ReadFile(path)
	if err != nil {
		return response, err
	}
	p.Write(b)

	response.SHA1 = hex.EncodeToString(hash1.Sum(nil))
	response.SHA256 = hex.EncodeToString(hash2.Sum(nil))

	return response, nil
}
