// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package endpoint

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/icza/gog"
	"gitlab.com/dan120/pcloud-emulator/archive"
	"gitlab.com/dan120/pcloud-emulator/model"
)

type Links struct {
	Archive archive.ArchiveService
}

func NewLinks(a archive.ArchiveService) Links {
	return Links{
		Archive: a,
	}
}

func (l *Links) CreateUploadLinkEndpoint(c *gin.Context) {
	var m *model.Metadata
	var found bool

	// get path and fileId params
	path := c.Query("path")
	fileId := GetInt64Value(c, "folderid")

	// try to find the file being referenced
	if fileId != nil {
		m, found = l.Archive.StatWithFileId(*fileId)
	} else {
		m, found = l.Archive.StatWithPath(path)
	}

	// only process if it is a found file
	if !found || !m.IsFolder {
		c.JSON(http.StatusOK, model.CreateErrorResponse(2009, gog.Ptr("File not found.")))
		return
	}

	// return response
	c.JSON(http.StatusOK, model.CreateUploadLinkResponse{
		UploadLinkId: 1,
		Link:         fmt.Sprintf("file://%s", m.FullPath),
	})
}

func (l *Links) GetFilePubLinkEndpoint(c *gin.Context) {
	response := model.GetFilePubLinkResponse{
		LinkId: 1,
	}

	c.JSON(http.StatusOK, response)
}
