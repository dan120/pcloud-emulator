// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package endpoint

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/icza/gog"
)

func GetInt64Value(c *gin.Context, key string) *int64 {
	var retval *int64
	if s, found := c.GetQuery(key); found {
		if i, err := strconv.Atoi(s); err == nil {
			retval = gog.Ptr(int64(i))
		}
	}
	return retval
}
