// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package archive

import "gitlab.com/dan120/pcloud-emulator/model"

type ArchiveService interface {
	CreateFolder(parentFolderId int64, name string) (*model.Metadata, error)
	CreateFolderIfNotExists(parentFolderId int64, name string) (*model.Metadata, error)
	ListFolderWithPath(path string) (*model.Metadata, error)
	ListFolderWithFolderId(folderId int64) (*model.Metadata, error)
	StatWithFileId(fileId int64) (metadata *model.Metadata, found bool)
	StatWithPath(path string) (metadata *model.Metadata, found bool)
	StatWithFullPath(path string) (metadata *model.Metadata, found bool)
}
