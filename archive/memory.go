// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package archive

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"log/slog"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/djherbis/times"
	"github.com/fsnotify/fsnotify"
	"github.com/hashicorp/go-memdb"
	"gitlab.com/dan120/pcloud-emulator/model"
)

type InMemoryArchive struct {
	Context    context.Context
	RootPath   string
	NextFileId int64
	Db         *memdb.MemDB
	Watcher    *fsnotify.Watcher
}

type File struct {
	Id         int64
	CreateTime time.Time
	ParentId   int64
	Name       string
	Path       string
	FullPath   string
	IsDir      bool
	Size       int64
}

func NewInMemoryArchive(ctx context.Context, rootPath string) (InMemoryArchive, error) {
	ima := InMemoryArchive{}

	if rootPath == "" {
		return ima, fmt.Errorf("root path is not defined")
	}

	if _, err := os.Stat(rootPath); errors.Is(err, os.ErrNotExist) {
		return ima, fmt.Errorf("root path does not exist: %s", rootPath)
	}

	// create the DB
	schema := &memdb.DBSchema{
		Tables: map[string]*memdb.TableSchema{
			"files": {
				Name: "files",
				Indexes: map[string]*memdb.IndexSchema{
					"id": {
						Name:    "id",
						Unique:  true,
						Indexer: &memdb.IntFieldIndex{Field: "Id"},
					},
					"path": {
						Name:    "path",
						Unique:  true,
						Indexer: &memdb.StringFieldIndex{Field: "Path"},
					},
					"parentid": {
						Name:    "parentid",
						Unique:  false,
						Indexer: &memdb.IntFieldIndex{Field: "ParentId"},
					},
				},
			},
		},
	}

	db, err := memdb.NewMemDB(schema)
	if err != nil {
		return ima, err
	}

	nextFileId := int64(0)

	// create watcher on root directory
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return ima, err
	}
	err = watcher.Add(rootPath)
	if err != nil {
		return ima, err
	}

	// set the InMemoryArchive struct properties
	ima.Context = ctx
	ima.RootPath = rootPath
	ima.Db = db
	ima.NextFileId = nextFileId
	ima.Watcher = watcher

	// add files to DB
	err = filepath.WalkDir(rootPath, func(fullPath string, d fs.DirEntry, err error) error {
		fi, e := d.Info()
		if e != nil {
			return e
		}
		_, e = ima.addPathToDB(fullPath, fi)
		if e != nil {
			return e
		}
		return nil
	})
	if err != nil {
		return ima, err
	}

	// start monitoring for file change events
	go ima.monitorWatcherEvents()

	slog.Info("In-memory archive initialized", "rootPath", rootPath)

	return ima, nil
}

func (p InMemoryArchive) CreateFolder(parentFolderId int64, name string) (*model.Metadata, error) {
	return p.createFolder(parentFolderId, name, true)
}

func (p InMemoryArchive) CreateFolderIfNotExists(parentFolderId int64, name string) (*model.Metadata, error) {
	return p.createFolder(parentFolderId, name, false)
}

func (p InMemoryArchive) createFolder(parentFolderId int64, name string, failOnExists bool) (*model.Metadata, error) {
	pMeta, found := p.StatWithFileId(parentFolderId)

	if !found {
		return nil, fmt.Errorf("parent folder %d for %s not found", parentFolderId, name)
	}

	if !pMeta.IsFolder {
		return nil, fmt.Errorf("parent id %d is not a folder", parentFolderId)
	}

	newPath := filepath.Join(pMeta.Path, name)
	newFullPath := filepath.Join(pMeta.FullPath, name)
	//var f File
	var retval *model.Metadata

	retval, found = p.StatWithPath(newPath)
	if !found {
		slog.Debug("Creating directory", "path", newFullPath)

		err := os.Mkdir(newFullPath, os.ModePerm)
		if err != nil {
			return nil, err
		}

		// wait for watcher to pick up the new directory
		found := false
		var m *model.Metadata
		for i := 0; i < 4 && !found; i++ {
			m, found = p.StatWithFullPath(newFullPath)
			if !found {
				time.Sleep(500 * time.Millisecond)
			}
		}

		// if it still couldn't be found, error out
		if !found {
			return nil, fmt.Errorf("unable to stat new directory")
		}

		retval = m
	} else if failOnExists {
		return nil, fmt.Errorf("folder already exists")
	}

	return retval, nil
}

func (p InMemoryArchive) ListFolderWithPath(path string) (*model.Metadata, error) {
	s, err := url.QueryUnescape(path)
	if err != nil {
		return nil, err
	}
	return p.listFolder("path", s)
}

func (p InMemoryArchive) ListFolderWithFolderId(folderId int64) (*model.Metadata, error) {
	return p.listFolder("id", folderId)
}

func (p InMemoryArchive) listFolder(index string, val interface{}) (*model.Metadata, error) {
	// get the folder metadata
	txn := p.Db.Txn(false)
	defer txn.Abort()

	raw, err := txn.First("files", index, val)
	if err != nil {
		return nil, err
	}

	if raw == nil {
		return nil, fmt.Errorf("unable to find folder: %v", val)
	}

	retfile := raw.(*File)
	retval := p.convertFileToMetadata(*retfile)

	// get all child files
	it, err := txn.Get("files", "parentid", retfile.Id)
	if err != nil {
		return nil, err
	}

	contents := make([]model.Metadata, 0)
	for obj := it.Next(); obj != nil; obj = it.Next() {
		f := obj.(*File)
		slog.Info("Got list result", "file", f)
		if f.Id != retfile.Id {
			contents = append(contents, p.convertFileToMetadata(*f))
		}
	}

	retval.Contents = contents

	return &retval, nil
}

func (p InMemoryArchive) StatWithFileId(fileId int64) (metadata *model.Metadata, found bool) {
	return p.stat("id", fileId)
}

func (p InMemoryArchive) StatWithPath(path string) (metadata *model.Metadata, found bool) {
	return p.stat("path", path)
}

func (p InMemoryArchive) StatWithFullPath(path string) (metadata *model.Metadata, found bool) {
	return p.stat("path", strings.TrimPrefix(path, p.RootPath))
}

func (p InMemoryArchive) stat(index string, val interface{}) (metadata *model.Metadata, found bool) {
	txn := p.Db.Txn(false)
	defer txn.Abort()

	raw, err := txn.First("files", index, val)
	if err != nil {
		slog.Error("Error retrieving file metadata from db", "error", err)
		return nil, false
	}

	if raw == nil {
		return nil, false
	}

	file := raw.(*File)
	m := p.convertFileToMetadata(*file)

	return &m, true
}

func (p InMemoryArchive) convertFileToMetadata(f File) model.Metadata {
	m := model.Metadata{
		Id:             fmt.Sprintf("%d", f.Id),
		FullPath:       f.FullPath,
		Created:        f.CreateTime.Format("Mon, 02 Jan 2006 15:04:05 -0700"),
		ParentFolderId: f.ParentId,
		Path:           f.Path,
		Name:           f.Name,
		IsFolder:       f.IsDir,
		IsMine:         true,
		Size:           f.Size,
	}
	if f.IsDir {
		m.FolderId = &f.Id
	} else {
		m.FileId = &f.Id
	}
	return m
}

func (p *InMemoryArchive) setSizeInDB(fullPath string, size int64) error {
	// build relative path
	relativePath := strings.TrimPrefix(fullPath, p.RootPath)
	if relativePath == "" {
		relativePath = "/"
	}

	txn := p.Db.Txn(true)
	defer txn.Abort()
	if fi, err := txn.First("files", "path", relativePath); err != nil {
		return err
	} else {
		fi.(*File).Size = size
	}
	txn.Commit()
	return nil
}

func (p *InMemoryArchive) addPathToDB(fullPath string, d fs.FileInfo) (int64, error) {
	// build relative path
	relativePath := strings.TrimPrefix(fullPath, p.RootPath)
	if relativePath == "" {
		relativePath = "/"
	}

	// determine parent folder id
	parentId := int64(0)
	if fullPath != p.RootPath {
		parentPath := path.Dir(relativePath)
		pm, found := p.StatWithPath(parentPath)
		if !found || pm == nil {
			return 0, fmt.Errorf("unable to find parent for %s", relativePath)
		}
		parentId = *pm.FolderId
	}

	newFileId := p.NextFileId

	t, err := times.Stat(fullPath)
	if err != nil {
		return 0, err
	}

	// create file struct
	f := &File{
		Id:         newFileId,
		CreateTime: t.BirthTime(),
		Name:       filepath.Base(relativePath),
		ParentId:   parentId,
		FullPath:   fullPath,
		Path:       relativePath,
		IsDir:      d.IsDir(),
	}

	// set file size
	if d.IsDir() {
		p.Watcher.Add(fullPath)
	} else {
		f.Size = d.Size()
	}

	// add to DB
	txn := p.Db.Txn(true)
	defer txn.Abort()
	slog.Info("Adding file", "file", f)
	if err := txn.Insert("files", f); err != nil {
		return 0, err
	}
	txn.Commit()
	p.NextFileId = p.NextFileId + 1

	return newFileId, nil
}

func (p *InMemoryArchive) removePathFromDB(fullPath string) error {
	slog.Info("removePathFromDB", "fullPath", fullPath)

	// create relative path
	path := strings.TrimPrefix(fullPath, p.RootPath)
	if path == "" {
		path = "/"
	}

	// get the DB record with the same relative path
	txn := p.Db.Txn(false)
	f, err := txn.First("files", "path", path)
	if err != nil {
		txn.Abort()
		return fmt.Errorf("unable to find file with path: %s", path)
	}
	txn.Abort()

	// remove the record from the DB if found
	if f != nil {
		txn = p.Db.Txn(true)
		defer txn.Abort()
		if err := txn.Delete("files", f); err != nil {
			return err
		}
		txn.Commit()
	}

	return nil
}

func (p InMemoryArchive) monitorWatcherEvents() {
	for {
		select {
		case event, ok := <-p.Watcher.Events:
			if !ok {
				return
			}
			slog.Debug("Watcher event", "event", event)
			if event.Has(fsnotify.Create) {
				if fi, err := os.Stat(event.Name); err == nil {
					if _, err = p.addPathToDB(event.Name, fi); err != nil {
						slog.Error("Error adding created file to database", "name", event.Name, "error", err)
					}
				} else {
					slog.Error("Stat error when adding created file to database", "name", event.Name, "error", err)
				}
			} else if event.Has(fsnotify.Remove) {
				if err := p.removePathFromDB(event.Name); err != nil {
					slog.Error("Error removing file from database", "name", event.Name, "error", err)
				}
			} else if event.Has(fsnotify.Write) {
				if fi, err := os.Stat(event.Name); err == nil {
					if err = p.setSizeInDB(event.Name, fi.Size()); err != nil {
						slog.Error("Error setting file size in database", "name", event.Name, "error", err)
					}
				} else {
					slog.Error("Error adding created file to database", "name", event.Name, "error", err)
				}
			}
		case err, ok := <-p.Watcher.Errors:
			if !ok {
				return
			}
			slog.Error("Watcher error", "error", err)
		}
	}
}
