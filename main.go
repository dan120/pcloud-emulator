// Copyright 2024 Daniel Noguerol. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/dan120/pcloud-emulator/archive"
	"gitlab.com/dan120/pcloud-emulator/endpoint"
)

func main() {
	ctx := context.Background()

	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug})))

	viper.AutomaticEnv()
	viper.SetDefault("PORT", "8080")

	// instantiate in-memory archive
	as, err := archive.NewInMemoryArchive(ctx, viper.GetString("ROOT_FOLDER"))
	if err != nil {
		slog.Error("Error creating archive", "error", err)
		os.Exit(1)
	}

	// set up Gin
	r := gin.Default()

	// set up routes
	folder := endpoint.NewFolder(as)
	file := endpoint.NewFile(as)
	links := endpoint.NewLinks(as)
	fileOps := endpoint.NewFileOps(as)
	r.GET("/createfolder", folder.CreateFolderEndpoint)
	r.GET("/createfolderifnotexists", folder.CreateFolderIfNotExistsEndpoint)
	r.GET("/listfolder", folder.ListFolderEndpoint)
	r.GET("/createuploadlink", links.CreateUploadLinkEndpoint)
	r.GET("/getfilepublink", links.GetFilePubLinkEndpoint)
	r.GET("/stat", file.StatEndpoint)
	r.POST("/uploadfile", file.UploadFileEndpoint)
	r.GET("/file_open", fileOps.FileOpenEndpoint)
	r.GET("/file_size", fileOps.FileSizeEndpoint)
	r.GET("/file_pread", fileOps.FilePreadEndpoint)
	r.GET("/file_close", fileOps.FileCloseEndpoint)

	// start the server
	r.Run(fmt.Sprintf(":%s", viper.GetString("PORT")))
}
